# Cartesian Genetic Programming for Videogames

This repository explains hwo to use CGP for playing videogames.



## Goal

The goal of this course is to develop an evolutionary algorithm for playing a videogame. Here, we use CGP as evolutionary algorithm.





## List of games

Here are some potential games to test your algorithm.







| Name                                                         | Programming Language                  |
| ------------------------------------------------------------ | ------------------------------------- |
| [Angry Brids](<https://aibirds.org/basic-game-playing-software.html>) | Java                                  |
| [Pong](https://geometrian.com/programming/projects/index.php?project=Ping%20Pang%20Pong) | Python (PyGame)                       |
| [Pong](https://www.101computing.net/pong-tutorial-using-pygame-adding-a-scoring-system/) | Python (PyGame) (has errors)          |
| [Breakout](https://developer.mozilla.org/en-US/docs/Games/Tutorials/2D_Breakout_game_pure_JavaScript) | HTML5 + Javascript                    |
| [Misc](http://phaser.io/examples)                            | HTML5 + Javascript (Phaser framework) |
|                                                              |                                       |
|                                                              |                                       |



## TODO

- [ ] Crear lista de resources (libros, links)
- [ ] Buscar como conectar breakout y python
- [ ] Buscar juego js: breakout, pong, space invaders
- [ ] Agregar ref de angry birds y buscar el juego
- [ ] 
- [ ] 
- [ ] 





**Breakout**



- [ ] Comunicar breakout (javascript) con python usando sockets o peticiones http
- [ ] Separar el código css y js de `index.html` para entenderlo mejor
- [ ] Entregar un CGP
- [ ] entrenar un cgp para breakout
- [ ] buscar 7 juegos para la clase
- [ ] descargar libros digitales cuando llegen y mandarselos al amigo
- [ ] cual será el reto de la tarea? que modifiquen el cgp (se los vamos a dar) con el codigo del juego (se los vamos a dar) para que lo entrenen (ellos decidiran qué variables usar y como usarlas)
- [ ] agregar el código de breakout en el repo `cgp_for_videogames`
- [ ] actualizar el paper con los comentarios del dr ishibuchi
- [ ] preparar slides de cgp para la clase
- [ ] hacer presentacion seminario y preparar speech
- [ ] mostrar avance de uso de cgp + breakout el jueves
- [ ] preparar la clase para el martes 17 de octubre



**Implementación de CGP en typescript**

- [ ] Crear directorio `cgp` en raiz
- [ ] Implementar `cgp.py` en typescript
- [ ] Integrar con `index.html` de breakout
- [ ] Probar entrenamiento
- [ ] Buscar mas juegos







**Estructura**

```
cgp_for_videogames
	games				example games
		breakout		html+js (este juego sera la base para que los alumnos entiendan la tarea, ie
						para mostrar como separar el main loop de draw() y como evolucionar el cgp)
		flappybird		python (este sera otro ejemplo pero en python)
		
		[more games]    los alumnos deberan encargarse de hacer la comunicacion entre el juego y cgp
                        (programado en typescript)
		
	cgp_typescript		implementation of cgp in typescript
```





## Running web2py

Required packages:

```
pip install yatl
pip install pydal
```

Run the server:

```
cd web2py
python3 web2.py.py
```







## Contact

Auraham Camacho `auraham.cg@gmail.com`