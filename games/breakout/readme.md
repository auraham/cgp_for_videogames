# Breakout

This directory contains an implementation of Breakout. To more details about this implementation, [follow this tutorial from Mozilla](https://developer.mozilla.org/en-US/docs/Games/Tutorials/2D_Breakout_game_pure_JavaScript).

[image]









## Scripts

| Name          | Description                |
| ------------- | -------------------------- |
| `index.html`  | Main file                  |
| `breakout.js` | Implementation of the game |
| `style.css`   | CSS styles                 |



## How to play

Open `index.html` in your browser.



## How to train 







## Start server

Requirements:

```
pip install flask
pip install flask_cors
```

Run server:

```
export FLASK_APP=server.py
export FLASK_ENV=development
flask run
```

Now, open `http://127.0.0.1:5000` in your browser.









## Futher improvements

- [ ] Replace `document.location.reload();` to reload the game.







## notes

How to use `fetch` to get data:

```javascript
// GitHub API URL
const url = 'https://api.github.com/users';

// make the API call
fetch(url)
    .then(res => res.json())
    .then(data => { console.log('Data: ', data); })
    .catch(err => { console.error('Error: ', err); });
```

How to use `fetch` to post data:

```javascript
const url = 'https://reqres.in/api/users';

// post body data 
const user = {
    first_name: 'John',
    last_name: 'Doe',
    job_title: 'Blogger'
};

// request options
const options = {
    method: 'POST',
    body: JSON.stringify(user),
    headers: {
        'Content-Type': 'application/json'
    }
}

// send POST request
fetch(url, options)
    .then(res => res.json())
    .then(res => console.log(res));
```



[source](https://attacomsian.com/blog/using-javascript-fetch-api-to-get-and-post-data)



We use `fetch` to create two functions:

- `sendScores()` para enviar los `scores` al servidor. El servidor responde con un `response: ok`
- `moveController()` para recibir la nueva posicion/accion del control/paddle del servidor. El servidor responde con un `x: pos`.





```javascript
// global
scores = [1,2,3,4,5,6,7,8,9,10]

function catlist(l) {
    
   
}

function sendScores() {
    
    const url = "localhost:8000/cgp_server/evolution";

    // request options
    const options = {
        method: 'POST',
        body: JSON.stringify(scores),
        headers: {
            'Content-Type': 'application/json'
        }
    }

    // send POST request
    fetch(url, options)
        .then(res => res.json())
        .then(res => console.log(res));
    
}

function moveController() {
    
    const url = "localhost:8000/cgp_server/action";

    // define state
    state = {
        x: x,   // ball x
        y: y,   // ball y
    }
    
    // request options
    const options = {
        method: 'POST',
        body: JSON.stringify(state),
        headers: {
            'Content-Type': 'application/json'
        }
    }

    // send POST request
    fetch(url, options)
        .then(res => res.json())
        .then(res => console.log(res));  // move paddle here
   
}
```









