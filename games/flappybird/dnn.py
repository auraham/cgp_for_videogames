# dnn.py
from __future__ import print_function
from keras import models
from keras import layers
import numpy as np
from numpy.random import RandomState


def load_dataset():
    """
    Load and split the dataset
    This function returns balanced sets of samples
    """
    
    rand = RandomState(100)
    
    # load dataset
    filename = "flappybird_samples.txt"
    data = np.genfromtxt(filename)
    y = data[:, 3]
    
    # split samples wrt the classes
    data_class_1 = data[y > 0]   # class 1 (just a few)
    data_class_0 = data[y < 1]   # class 0 (a lot of them)
    
    # get number of positive samples
    n_samples, _ = data_class_1.shape
    
    # reduce the number of negative samples
    to_keep = rand.randint(0, n_samples+1, (n_samples, ))
    data_class_0 = data_class_0[to_keep]
    
    # merge and shuffle
    data = np.vstack((data_class_1, data_class_0))
    n_samples, _ = data.shape
    perm = rand.permutation(n_samples)
    data = data[perm]
    
    X = data[:, :3]
    y = data[:, 3]
        
    # split dataset
    X_train = X[:5000]
    y_train = y[:5000]
    
    X_val = X[5000:7000]
    y_val = y[5000:7000]
    
    X_test = X[7000:]
    y_test = y[7000:]
    
    return X_train, y_train, X_test, y_test, X_val, y_val
    
    


if __name__ == "__main__":
    
    # config
    save = False

    # load dataset
    X_train, y_train, X_test, y_test, X_val, y_val = load_dataset()
    
    # model definition
    model = models.Sequential()
    model.add(layers.Dense(16, activation="relu", input_shape=(3, )))
    model.add(layers.Dense(16, activation="relu"))
    model.add(layers.Dense(1, activation="sigmoid"))
    
    # compile
    model.compile(optimizer="rmsprop",
                loss="binary_crossentropy",
                metrics=["accuracy"])
                
    # training
    hist = model.fit(X_train, y_train, epochs=10, batch_size=128, validation_data=(X_val, y_val))
    
    # print info
    hist_dict = hist.history
    print(hist_dict)
    

    # save model
    if save:
        filename = "model_dnn.h5"
        model.save(filename)
        print("saving model:", filename)
    
