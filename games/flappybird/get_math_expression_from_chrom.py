# get_math_expression_from_chrom.py
from cgp import load_chrom


def get_arity(node):
    
    return sum([1 for val in node.i_inputs if val is not None])

def eval(index, nodes, fs, leaves):
    """
    @pending
    Evaluate a node
    
    Input
    index       int, node id (negative values are for terminal nodes)
    fs          list of functions
    leaves      dictionary of terminal nodes
    """
    
    
    # base case
    if index < 0:
        return leaves[index]
    
    
    # current node
    node = nodes[index]
    arity = get_arity(node)
    
    if arity == 1:
        
        right = eval(node.i_inputs[0], nodes, fs, leaves) * node.weights[0]
        oper  = fs[node.i_func]
        
        return (oper, right)
        
    if arity == 2:
        
        left  = eval(node.i_inputs[0], nodes, fs, leaves) * node.weights[0]
        right = eval(node.i_inputs[1], nodes, fs, leaves) * node.weights[1]
        oper  = fs[node.i_func]
        
        return (left, oper, right)
        
        
def unfold(index, nodes, fs, leaves):
    """
    Return a string representation of the graph
    
    Input
    index       int, node id (negative values are for terminal nodes)
    fs          list of functions (encoded as strings)
    leaves      dictionary of terminal nodes (encoded as strings)
    
    Return
    repr        str, string representation of the graph
    """
    
    # base case
    if index < 0:
        return leaves[index]
    
    
    # current node
    node = nodes[index]
    arity = get_arity(node)
    
    if arity == 1:
        
        r_v = unfold(node.i_inputs[0], nodes, fs, leaves)   # right value
        r_w = node.weights[0]                               # right weight
        op  = fs[node.i_func]                               # operator
        
        return "(%s %s * %.5f)" % (op, r_v, r_w)
        
        
    if arity == 2:
        
        l_v  = unfold(node.i_inputs[0], nodes, fs, leaves)  # left value
        l_w  = node.weights[0]                              # left weight
        
        r_v = unfold(node.i_inputs[1], nodes, fs, leaves)   # right value
        r_w = node.weights[1]                               # right weight
        
        op  = fs[node.i_func]                               # operator
        
        return "((%s * %.5f) %s (%s * %.5f))" % (l_v, l_w, op, r_v, r_w)
    

if __name__ == "__main__":
    
    """
    saving best_chrom_t_6.pkl
    reference
    [fitness: 3896
     nodes (10): 
        0: (i_func: 4, i_inputs: [   -2,  None], weights: [-0.26,  None], i_output:  0, output:    None, active: False)
       *1: (i_func: 4, i_inputs: [   -2,  None], weights: [-0.19,  None], i_output:  1, output:    7.31, active: True)
        2: (i_func: 0, i_inputs: [   -1,    -3], weights: [ 0.68, -0.47], i_output:  2, output:    None, active: False)
       *3: (i_func: 4, i_inputs: [   -3,  None], weights: [ 0.58,  None], i_output:  3, output:  -82.53, active: True)
       *4: (i_func: 0, i_inputs: [   -3,    -1], weights: [ 0.86, -0.63], i_output:  4, output:   20.63, active: True)
        5: (i_func: 3, i_inputs: [    1,     0], weights: [ 0.62, -0.35], i_output:  5, output:    None, active: False)
       *6: (i_func: 1, i_inputs: [    1,     3], weights: [-0.32,  0.31], i_output:  6, output:   23.41, active: True)
        7: (i_func: 1, i_inputs: [    4,     5], weights: [ 0.49, -0.72], i_output:  7, output:    None, active: False)
        8: (i_func: 0, i_inputs: [    1,     7], weights: [-0.84,  0.59], i_output:  8, output:    None, active: False)
       *9: (i_func: 1, i_inputs: [    6,     4], weights: [-0.56, -0.19], i_output:  9, output:   -9.30, active: True)
    ]
    """
    
    filepath = "best_chrom_t_6.pkl"
    chrom = load_chrom(filepath)
    
    fs = ["+", "-", "*", "/", "-"]
    leaves = {-1: "a", -2: "b", -3: "c"}
    
    
    import ipdb; ipdb.set_trace()
    
    result = unfold(9, chrom.nodes, fs, leaves)
    
    
    
    
    
    
    
    
