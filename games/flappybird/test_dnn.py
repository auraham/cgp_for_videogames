# test_dnn.py
from __future__ import print_function
from keras import models
from keras import layers
from keras.models import load_model
from dnn import load_dataset
import numpy as np
from cgp import load_chrom
from unfold import unfold, get_last_active_node

if __name__ == "__main__":
    
    # load model
    model = load_model("model_dnn.h5")
    
    # load dataset
    X_train, y_train, X_test, y_test, X_val, y_val = load_dataset()
    
    # predict some actions
    predictions = model.predict(X_test)
    
    # reshape and cast using threshold
    pred_dnn = predictions.flatten()
    mask = pred_dnn > 0.5
    pred_dnn[mask]  = 1
    pred_dnn[~mask] = 0
    
    # check accuracy
    n = len(y_test)
    error = (y_test != pred_dnn).sum() / n
    print("missclassified (dnn) %.4f" % error)
    
    # load cgp
    filename = "best_chrom_t_6.pkl"
    chrom = load_chrom(filename)
    
    # prediction from cgp
    n = len(pred_dnn)
    pred_cgp = np.zeros((n, ))
    
    for i in range(n):
        
        v,h,g = X_test[i]           # get input variables
        y = chrom.eval(v,h,g)       # eval chrom
        action = 0
        if y > 0:
            action = 1
        
        pred_cgp[i] = action        # save action
    
    # check accuracy
    n = len(y_test)
    error = (y_test != pred_cgp).sum() / n
    print("missclassified (cgp) %.4f" % error)
    
    # unfold
    fs = ["+", "-", "*", "/", "-"]          # function set
    leaves = {-1: "v", -2: "h", -3: "g"}    # terminal nodes
    
    last_node = get_last_active_node(chrom.nodes)
    result = unfold(last_node, chrom.nodes, fs, leaves)
    
    
