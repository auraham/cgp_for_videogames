# Notes

This document contains some notes about this project to know how it works.



## Main loop

This loops controls what we see every time the birds are flying:

```python
# main.py

def main():
    game = Game()
    while game.running and game.current_generation <= N_GEN:
        
        # clear all sprites
        # place self.n_birds randomly in the screen
        # place the pipes
        # draw the background
        game.reset()
        
        # start the game and the evolutionary process
        game.run()
```



## Update the screen

The birds and the pipes moves towards the left as long as there is at least one bird alive. The screen is updated according to this sequence of functions:

```python
# main.py

def main():
    game = Game()
    while game.running and game.current_generation <= N_GEN:
        
        # clear all sprites
        # place self.n_birds randomly in the screen
        # place the pipes
        # draw the background
        game.reset()
        
        # start the game and the evolutionary process
        game.run()
```

Then, go to `run()`:

```python
def run(self):
    self.playing = True
    while self.playing:
        self._handle_events()			# listener for key events
        self._update()					# update the locations of the pipes, so the birds seem to fly
        self._draw()                    # update the screen (move all objects to the left)
        self._clock.tick(self._fps)
    if not self.running:
        return
        
        # ...
        # evolution goes here ...
        # self.pop = cgp.evolve(self.pop, pb, MU, LAMBDA)
```

The important functions in this loop are `self._update()` and `self._draw()`. 

First, `self._update()` changes the locations of the pipes backwards (i.e., to the left), so the birds seem to fly. If there are no birds alive, then this function flips up a flag and the game is over.

```python
def _update(self):
    """
    Update the state (position, life, etc.) of all sprites and the game
    """
    
    self.all_sprites.update()
        
    # if all birds died, then game over
    if not self.birds:
        self.playing = False
        return
    
    # move the pipes backwards such that birds seem to fly
    leading_bird = max(self.birds, key=lambda b: b.rect.x)
    if leading_bird.rect.x < SCREEN_WIDTH / 3:
        for bird in self.birds:
            bird.moveby(dx=BIRD_X_SPEED)
    else:
        for pipe in self.pipes:
            pipe.moveby(dx=-BIRD_X_SPEED)
            if pipe.rect.x < -50:
           		pipe.kill()
    
    # count the score: one point per frame
    for bird in self.birds:
    	bird.score += 1  

    self._max_score += 1
    self._max_score_so_far = max(self._max_score_so_far, self._max_score)
    # spawn a new pipe if necessary
    while self._front_pipe.rect.x < SCREEN_WIDTH:
        self._spawn_pipe()
```

Notice that this function also updates the fitness of the birds. If the bird is still alive in the current frame, its fitness is incremented one unit:

```python
# count the score: one point per frame
for bird in self.birds:
	bird.score += 1  
```

You can check both the score and distance of the birds as follows:

```
ipdb> [bird.score for bird in self.birds]                                     
[6, 6, 6, 6, 6, 6, 6, 6, 6, 6]
```

```
ipdb> [bird.rect.x for bird in self.birds]                                    
[200, 56, 187, 91, 88, 40, 180, 24, 84, 78]
```

-----

**Note** The birds are stored in `self.birds`. Although it is iterable, it does not support indexing. Thus, you cannot access to the `i`-th bird using `self.birds[i]`:

```
ipdb> self.birds                                                              
<Group(10 sprites)>
ipdb> self.birds[0]                                                           
*** TypeError: 'Group' object does not support indexing
```

-----

On the other hand, `self._draw()` configures the text on the screen (current score, current generation, number of birds alive, etc.) . After defining all this, the screen is updated after calling `pg.display.update()`:

```python
def _draw(self):
    
    # new locations of the birds for the next time step
    self.all_sprites.draw(self._screen)

    # show score
    # ...

    # refresh screen
    pg.display.update()
```



## Evolutionary process

The evolutionary process takes place in `run()`:

```python
# game.py

def run(self):
    
    # this block handles the animation
    self.playing = True
    while self.playing:
        self._handle_events()
        self._update()
        self._draw() 
        self._clock.tick(self._fps)
    if not self.running:
        return
        
    # one generation finished and perform evolution again
    # if current score is very low, then we use a large mutation rate
    pb = MUT_PB
    if self._max_score < 500:
        pb = MUT_PB * 3
    elif self._max_score < 1000:
        pb = MUT_PB * 2
    elif self._max_score < 2000:
        pb = MUT_PB * 1.5
    elif self._max_score < 5000:
        pb = MUT_PB * 1.2
    
    # evolutionary process
    self.pop = cgp.evolve(self.pop, pb, MU, LAMBDA)
```

The first blocks handles the animation. That is, it moves the pipes backwards. Also, it updates the fitness of the birds. If all the birds failed, then the game is over.

```python
# game.py

def run(self):
    
    # this block handles the animation
    self.playing = True
    while self.playing:
        self._handle_events()
        self._update()
        self._draw() 
        self._clock.tick(self._fps)
    if not self.running:
        return
```

When the game is over, the evolutionary process starts. To do so, we first tune the mutation rate:

```python
# game.py

def run(self):
    
    # ... animation ...
    
    # one generation finished and perform evolution again
    # if current score is very low, then we use a large mutation rate
    pb = MUT_PB
    if self._max_score < 500:
        pb = MUT_PB * 3
    elif self._max_score < 1000:
        pb = MUT_PB * 2
    elif self._max_score < 2000:
        pb = MUT_PB * 1.5
    elif self._max_score < 5000:
        pb = MUT_PB * 1.2
```

Before going any further, let us introduce some concepts: generation/round and scores.

A **generation** is called a *round* in the code. One generation comprises three steps, described below in the `run()` function.

```python
def run(self):
    
    # 1. play the game, and wait it is over
    
    # 2. when the game is over, configure mutation rate
   
    # 3. create a new populations of birds to play the game in the next round
```

So, as you expect, in order to train/find/evolve a good player/bird, we need several rounds/generations. A new population of birds is given every generation. The number of rounds/generations is given in `settings.py`:

```python
# settings.py
# parameters of evolutionary strategy: MU+LAMBDA
MU = 2
LAMBDA = 8
N_GEN = 200  # max number of generations
```

and the function `run()` is called from `main.py`:

```python
# main.py

def main():
    game = Game()
    while game.running and game.current_generation <= N_GEN:
        game.reset()
        game.run()
```

Every iteration of this loop is a generation of the evolutionary process. On the other hand, there are two scores in the code, 

```python
# game.py

self._max_score_so_far = 0  # max score so far in all the rounds since the game started
self._max_score = 0         # max score of all the birds in this round (generation)
```

That is, `self._max_score` is the maximum score (among) among all the birds in the current generation. From our example above:

```python
ipdb> [bird.score for bird in self.birds]                                     
[6, 6, 6, 6, 6, 6, 6, 6, 6, 6]
```

`self._max_score` will be `6`. However, as the evolutionary algorithm creates better solutions, the fitness is expected to increase. Here, `self._max_score_so_far` saves the maximum score for all the birds since the game started. Suppose that the maximum score in generation `self.current_generation=10` is `100`:

```python
# scores in generation 100
ipdb> [bird.score for bird in self.birds]                                     
[40, 50, 60, 100, 80, 90, 40, 56, 35, 74]
```

Then, all the birds die and the game is restarted again. In the next generation, we can have this:

```python
# scores in generation 101
ipdb> [bird.score for bird in self.birds]                                     
[60, 20, 12, 23, 42, 54, 65, 76, 87, 12]
```

Notice that any of these birds have a score higher than `100`, as in the previous generation. The reason is as follows. Although the evolutionary algorithm is elitist (the fittest solution is always preserved), the score does not necessarily increases over time. This is because the environment changes every generation. Every time all the birds die, a new population is created (preserving the best `MU=2` solutions), the environment is created again (i.e., the locations and gaps of the pipes are created and they are not necessarily the same as in the previous generation), and the birds fly in the new environment. Since the configuration of the pipes changes every generation, we cannot expect that the fitness of the best individual/bird (`self._max_score`) increases over time. However, the best fitness gathered since the start of the game (`self._max_score_so_far`) must increase over time.

 Finally, we can evolve a population of birds:

```python
def run(self):
    
    # 1. play the game, and wait it is over
    
    # 2. when the game is over, configure mutation rate
    
    # 3. create a new populations of birds to play the game in the next round
    self.pop = cgp.evolve(self.pop, pb, MU, LAMBDA)
```

Let us take a look at the population:

```python
ipdb> self.pop                                                                
[<cgp.Individual object at 0x7f0210db3860>, 
<cgp.Individual object at 0x7f01e1899748>, 
<cgp.Individual object at 0x7f01e18406a0>, 
<cgp.Individual object at 0x7f01e18675f8>, 
<cgp.Individual object at 0x7f01e180f550>, 
<cgp.Individual object at 0x7f01e17b64a8>, 
<cgp.Individual object at 0x7f01e17dd400>, 
<cgp.Individual object at 0x7f01e1785358>, 
<cgp.Individual object at 0x7f01e17ac2b0>, 
<cgp.Individual object at 0x7f01e1754208>]
```

The population is a list of instances of `Individual`, defined in `cgp.py`:

```python
# cgp.py

class Individual:
    """
    An individual (chromosome, genotype, etc.) in evolution
    """
    function_set = None
    weight_range = [-1, 1]
    max_arity = 3
    n_inputs = 3
    n_outputs = 1
    n_cols = N_COLS
    level_back = LEVEL_BACK
```

Each individual contains a list of nodes (the number of nodes is determined by `N_COLS` in `settings.py`):

```python
ipdb> len(self.pop[0].nodes)                                          
500
```

Each node contains six attributes:

```python
# input function
ipdb> self.pop[0].nodes[0].i_func
1

# input parameters
ipdb> self.pop[0].nodes[0].i_inputs                                                     
[-1, -3]

# weights
ipdb> self.pop[0].nodes[0].weights                        
[0.6693339647391277, 0.027700185009203127]

# ?
ipdb> self.pop[0].nodes[0].i_output                                                
0

# output
ipdb> self.pop[0].nodes[0].output                                
201.0721387094124

# is this node active?
ipdb> self.pop[0].nodes[0].active                                                 
True
```



## Representation

In this section, we will talk about the representation of solutions. From [this tutorial](http://cs.ijs.si/ppsn2014/files/slides/ppsn2014-tutorial3-miller.pdf) given by Julian Miller (author of CGP):

> What defines CGP?
>
> - The genotype is a list of integers (and possibly parameters) that represent the program primitives and how the are connected together.
>   - CGP represents programs as *graphs* in which there are *non-coding genes*.
> - The genes are:
>   - Addresses in data (connection genes)
>   - Addresses in a look up table of functions
>   - Additional parameters
> - This representation is very simple, flexible, and convenient for many problems.







## How to print solutions

Let's add support to print a solution. Here, a solution is encoded as a set of linked nodes. First, add a breakpoint just before evolving the population:

```python
# game.py

def run(self):
        
    # add breakpoint
	if self.current_generation == 1:
        import ipdb; ipdb.set_trace()
            
    self.pop = cgp.evolve(self.pop, pb, MU, LAMBDA)
```

Now, run `main.py` from `ipython`:

```
%run main.py
```

The game will block/stop when all birds loss. Let's print the set of functions available for each node:

```
ipdb> self.pop[0].function_set                                                                                                                                
[<cgp.Function object at 0x7f8c0ffe8cf8>, <cgp.Function object at 0x7f8c0ffe8d68>, <cgp.Function object at 0x7f8c0ffe8eb8>, <cgp.Function object at 0x7f8c0ffe8ef0>, <cgp.Function object at 0x7f8c0ffe8f28>]

ipdb> len(self.pop[0].function_set)                                                                                                                           
5
```

So, there are five functions, but we still do not what functions they are. Modify the `Function` class as follows:

```python
# cgp.py
class Function:
    
    def __init__(self, name, f, arity):
        self.name = name
        self.f = f
        self.arity = arity
    
    def __call__(self, *args, **kwargs):
        return self.f(*args, **kwargs)
    
    def __repr__(self):
        return "[%s,%d]" % (self.name, self.arity)
```

and modify this block to use the new attribute `name`:

```python
fs = [Function("add", op.add, 2), 
      Function("sub", op.sub, 2), 
      Function("mul", op.mul, 2), 
      Function("div", protected_div, 2), 
      Function("neg", op.neg, 1)]
Individual.function_set = fs
```

Next, restart the game and print the first solution in the population:

```
ipdb> self.pop[0].function_set                                                                                                                                
[[add,2], [sub,2], [mul,2], [div,2], [neg,1]]
```

Now, it is easier to understand what functions are in the set. The class `Individual` has other attributes. However, only three of them are modified during the search process: `self.nodes`, `self.fitness`, and `self._active_determined`:

```python
# cgp.py

class Individual:
   
	# these attributes are not supposed to be modified after
    # giving them a value
    function_set = None
    weight_range = [-1, 1]
    max_arity = 3
    n_inputs = 3
    n_outputs = 1
    n_cols = N_COLS
    level_back = LEVEL_BACK
    
    # fields defined in __init__
    # these attributes are modified throughout the search process
    self.nodes = []
    self.fitness = None
    self._active_determined = False
    
```

In the following, we will add `__repr__` into `Individual` to print these three attributes.

```python
# cgp.py

class Individual:
    
    def __repr__(self):
        return "[fitness: %d, nodes: %s]" % (self.fitness, self.nodes)
```

Test this function:

```
ipdb> self.pop[0]
```

This is the output:

```
[fitness: 35, nodes: [<cgp.Node object at 0x7f11946f0668>, <cgp.Node object at 0x7f11941e59b0>, <cgp.Node object at 0x7f11740aa0b8>, <cgp.Node object at 0x7f11740aa080>, <cgp.Node object at 0x7f11740aa048>, <cgp.Node object at 0x7f11740aa0f0>, <cgp.Node object at 0x7f11740aa128>, <cgp.Node object at 0x7f11740aa160>, <cgp.Node object at 0x7f11740aa320>, <cgp.Node object at 0x7f11740b1c50>]]
```

As it can be seen, only the first attribute, `fitness`, is readable, whereas the others are unclear. To ease reading, we still need to add a `__repr__`  function in the`Node` class:

```python
# cgp.py

class Node:
    
    def __init__(self, max_arity):
        
        self.i_func = None
        self.i_inputs = [None] * max_arity
        self.weights = [None] * max_arity
        self.i_output = None
        self.output = None
        self.active = False
```

Add the following function:

```python
# cgp.py

class Node:
    
    def __repr__(self):
        return "(i_func: %s, i_inputs: %s, weights: %s, i_ouput: %s, output: %s, active: %s)" % 
               (self.i_func,
                self.i_inputs,
                self.weights,
                self.i_output,
                self.output,
                self.active,
               )
```

After this change, we can print a solution as follows:

```
ipdb> self.pop[0]  
```

This is the output:

```
[fitness: 35, nodes: [(i_func: 1, i_inputs: [-2, -3], weights: [-0.0901459909719573, 0.4110264538680559], i_output: 0, output: None, active: False), (i_func: 2, i_inputs: [0, -3], weights: [0.013730046153373365, 0.0658028292851427], i_output: 1, output: None, active: False), (i_func: 0, i_inputs: [0, -2], weights: [-0.473167314411886, -0.3292984481199799], i_output: 2, output: None, active: False), (i_func: 2, i_inputs: [-2, -2], weights: [0.9217207458385939, -0.622810171651029], i_output: 3, output: None, active: False), (i_func: 2, i_inputs: [-1, 3], weights: [0.25264327838559475, -0.5783320158262997], i_output: 4, output: None, active: False), (i_func: 3, i_inputs: [1, 3], weights: [0.8023040859747845, -0.6791563800501323], i_output: 5, output: None, active: False), (i_func: 0, i_inputs: [-1, 3], weights: [-0.9887098282164015, -0.7047910128012196], i_output: 6, output: None, active: False), (i_func: 4, i_inputs: [-1, None], weights: [-0.6147186880258533, None], i_output: 7, output: None, active: False), (i_func: 0, i_inputs: [1, 4], weights: [0.9402847803859782, -0.7467695828695236], i_output: 8, output: None, active: False), (i_func: 4, i_inputs: [-1, None], weights: [0.2182004637582724, None], i_output: 9, output: -2.618405565099269, active: True)]]
```

A bit messy yet. Let's add update `__repr__` of `Node` as follows:

```python
# cgp.py

class Node:
	def __repr__(self):
        
        str_i_inputs = format_vector(self.i_inputs)
        str_weights = format_vector(self.weights)
        str_i_output = "%2d" % self.i_output
        str_output = format_value(self.output)
        str_active = "%s" % self.active
        
        str_node = "(i_func: %s, i_inputs: %s, weights: %s, i_output: %s, output: %s, active: %s)" % (self.i_func, str_i_inputs, str_weights, str_i_output, str_output, str_active)
        
        return str_node
```

The functions `format_vector` and `format_value` are defined in `format_string.py`. Restart the game and print a single individual:

```
ipdb> self.pop[0]   
```

This is the output:

```
[fitness: 35
 nodes (10): 
    0: (i_func: 1, i_inputs: [   -2,    -3], weights: [-0.09,  0.41], i_output:  0, output:    None, active: False)
    1: (i_func: 2, i_inputs: [    0,    -3], weights: [ 0.01,  0.07], i_output:  1, output:    None, active: False)
    2: (i_func: 0, i_inputs: [    0,    -2], weights: [-0.47, -0.33], i_output:  2, output:    None, active: False)
    3: (i_func: 2, i_inputs: [   -2,    -2], weights: [ 0.92, -0.62], i_output:  3, output:    None, active: False)
    4: (i_func: 2, i_inputs: [   -1,     3], weights: [ 0.25, -0.58], i_output:  4, output:    None, active: False)
    5: (i_func: 3, i_inputs: [    1,     3], weights: [ 0.80, -0.68], i_output:  5, output:    None, active: False)
    6: (i_func: 0, i_inputs: [   -1,     3], weights: [-0.99, -0.70], i_output:  6, output:    None, active: False)
    7: (i_func: 4, i_inputs: [   -1,  None], weights: [-0.61,  None], i_output:  7, output:    None, active: False)
    8: (i_func: 0, i_inputs: [    1,     4], weights: [ 0.94, -0.75], i_output:  8, output:    None, active: False)
    9: (i_func: 4, i_inputs: [   -1,  None], weights: [ 0.22,  None], i_output:  9, output:   -2.62, active: True)
]
```

That's better. Now, let's explain how to evaluate the nodes of an individual.



## Evaluate nodes

As you remember, an individual encodes a graph. This graph is composed by a set of nodes. In this example, an individual contains ten nodes:

```python
def eval(self, *args):
       
	if not self._active_determined:
        self._determine_active_nodes()
        self._active_determined = True
            
    # add breakpoint
    import ipdb; ipdb.set_trace()
            
    # forward pass: evaluate
    for node in self.nodes:
        # evaluate node
```

```
ipdb> print(self)
```

```
[fitness: None
 nodes (10): 
   *0: (i_func: 1, i_inputs: [   -1,    -3], weights: [-0.15, -0.37], i_output:  0, output:    None, active: True)
    1: (i_func: 1, i_inputs: [   -1,    -3], weights: [-0.46, -0.11], i_output:  1, output:    None, active: False)
    2: (i_func: 0, i_inputs: [   -2,    -2], weights: [ 0.82, -0.06], i_output:  2, output:    None, active: False)
    3: (i_func: 4, i_inputs: [    2,  None], weights: [-0.92,  None], i_output:  3, output:    None, active: False)
    4: (i_func: 4, i_inputs: [    0,  None], weights: [ 0.95,  None], i_output:  4, output:    None, active: False)
    5: (i_func: 0, i_inputs: [   -2,     3], weights: [-0.01,  0.98], i_output:  5, output:    None, active: False)
    6: (i_func: 1, i_inputs: [    0,     0], weights: [-0.13, -0.47], i_output:  6, output:    None, active: False)
    7: (i_func: 1, i_inputs: [    1,     3], weights: [-0.20, -0.48], i_output:  7, output:    None, active: False)
    8: (i_func: 0, i_inputs: [    2,     6], weights: [-0.52, -0.65], i_output:  8, output:    None, active: False)
   *9: (i_func: 3, i_inputs: [    0,     0], weights: [ 0.44, -0.66], i_output:  9, output:    None, active: True)
]
```

-----

**Note** I do not the reason, but if you modify this part:

```
# init function set
fs = [
    Function("add", op.add, 2), 
    Function("sub", op.sub, 2), 
    Function("mul", op.mul, 2), 
    Function("div", protected_div, 2), 
    Function("neg", op.neg, 1),
    ]
        
Individual.function_set = fs
Individual.max_arity = max(f.arity for f in fs)
```

and put the function set just before the `Node` class (to access `fs` in `__repr__`), the output of `print(self)` changes. That is, there is something in the code that alters the initialization of individuals. The revision `6818afef` returns the same output as the repository on bitbucket (see **Reference Output** below).

**Note** The reason may be running the game within the same session of `ipython`. For instance, if you run the game two times in a row from `ipython`:

```
%run main.py    
%run main.py
```

then, the output of the two runs will be different, despite the fact that we are using the same seed. What does it means? That our current approach for setting the seed is not working:

```python
# cgp.py
# init PRNG
random.seed(SEED)
```

```python
# game.py
# init PRNG
random.seed(SEED)
```

----

Notice that:

- Each line represent a node.
- Some nodes are **active**. Active nodes are identified with a `*`.
- Each node contains inputs (`i_inputs`), weights to change the inputs (`weights`), and outputs (explained later).
- The output of each node is `None` because it has not been evaluated yet.

In the following, we explain how to evaluate a node. Consider the first active node only:

```
*0: (i_func: 4, i_inputs: [   -1,  None], weights: [-0.84,  None], i_output:  0, output:    None, active: True)
```

For completeness, the list of functions is:

```
ipdb> print(fs)                                           
[[add,2], [sub,2], [mul,2], [div,2], [neg,1]]
```

------

**Note** The function `eval()` of the class `Individual` is called from the function `eval()` of the class `AIBird`:

```python
# sprites.py
class AIBird(Bird):

    def eval(self, v, h, g):
        return self.brain.eval(v, h, g)   # this calls
```

```python
# cgp.py
class Individual:
	def eval(self, *args):
        # eval node
        # here, args is a tuple (v, h, g)
```

-----

This block evaluate the node:

```python
def eval(self, *args):
	# forward pass: evaluate
    for node in self.nodes:
        if node.active:
            inputs = []
            
            # for each input
            for i in range(self.function_set[node.i_func].arity):
                i_input = node.i_inputs[i]
                w = node.weights[i]
                if i_input < 0:
                    inputs.append(args[-i_input - 1] * w)
                else:
                    inputs.append(self.nodes[i_input].output * w)
            node.output = self.function_set[node.i_func](*inputs)
     return self.nodes[-1].output
```

where:

```
args = (207, 187, 111)
```

Notice that the function of the node is `neg`, so its arity is `1`.:

```python
# one input only
for i in range(self.function_set[node.i_func].arity):
    i_input = node.i_inputs[i]
    w = node.weights[i]
```

As a result, there is only one non-`None` entry in `i_inputs` and `weights`. Until this part, we have:

```
i_input = -1
w = -0.8392339728791409
```

Now, the tricky part:

```python
if i_input < 0:
	inputs.append(args[-i_input - 1] * w)
else:
	inputs.append(self.nodes[i_input].output * w)        
```

The mapping between `i_inputs` to `inputs` is as follows:

| `i_input` | `args[-(i_input) - 1]` | args[val] | Description           |
| --------- | ---------------------- | --------- | --------------------- |
| -1        | `args[-(-1) - 1]`      | `args[0]` | First argument (`v`)  |
| -2        | `args[-(-2) - 1]`      | `args[1]` | Second argument (`h`) |
| -3        | `args[-(-3) - 1]`      | `args[2]` | Third argument (`g`)  |
| 0         |                        |           |                       |
| 1         |                        |           |                       |
| 2         |                        |           |                       |

That is **the range of `i_input` is `[-number_of_arguments, number_of_nodes-1]`** (**check this line**). When `i_inputs >= 0`, the mapping changes as follows:

| `i_input` | `self.nodes[i_input].output` | Description                                                  |
| --------- | ---------------------------- | ------------------------------------------------------------ |
| 0         | `self.nodes[0].output`       | The output of node `self.nodes[0]` is the input of the current node. |
| 1         | `self.nodes[1].output`       | The output of node `self.nodes[1]` is the input of the current node. |
|           |                              |                                                              |
|           |                              |                                                              |
|           |                              |                                                              |
|           |                              |                                                              |

Bask to our example. The next line is:

```
node.output = self.function_set[node.i_func](*inputs)
```

where:

```
inputs = [-173.72143238598218]
```

As the function is `neg`, the output is:

```
node.output = 173.72143238598218
```

We can now print the list of nodes:

```
ipdb> print(self)                                                                                                                                             
[fitness: None
 nodes (10): 
   *0: (i_func: 4, i_inputs: [   -1,  None], weights: [-0.84,  None], i_output:  0, output:  173.72, active: True)
    1: (i_func: 2, i_inputs: [   -3,    -3], weights: [-0.42, -0.10], i_output:  1, output:    None, active: False)
    2: (i_func: 4, i_inputs: [   -1,  None], weights: [ 0.62,  None], i_output:  2, output:    None, active: False)
    3: (i_func: 3, i_inputs: [    0,     1], weights: [ 0.27,  0.69], i_output:  3, output:    None, active: False)
    4: (i_func: 1, i_inputs: [    0,     3], weights: [-0.72,  0.28], i_output:  4, output:    None, active: False)
    5: (i_func: 3, i_inputs: [   -3,     3], weights: [-0.95,  0.78], i_output:  5, output:    None, active: False)
   *6: (i_func: 4, i_inputs: [    0,  None], weights: [-0.71,  None], i_output:  6, output:    None, active: True)
    7: (i_func: 1, i_inputs: [    4,     3], weights: [ 0.67, -0.38], i_output:  7, output:    None, active: False)
    8: (i_func: 4, i_inputs: [    4,  None], weights: [-0.23,  None], i_output:  8, output:    None, active: False)
   *9: (i_func: 4, i_inputs: [    6,  None], weights: [-0.77,  None], i_output:  9, output:    None, active: True)
]
```

Notice that only the first one is evaluated.

The next active node is the node `6`:

```
*6: (i_func: 4, i_inputs: [    0,  None], weights: [-0.71,  None], i_output:  6, output:    None, active: True)
```

As it can be seen, this node uses `neg` and is connected to node `0`:

```python
i_func: 4,                 # function: neg
i_inputs: [    0,  None]   # ie [self.nodes[0].ouput,  None]
```

So:

```python
for i in range(self.function_set[node.i_func].arity):
	i_input = node.i_inputs[i]
    w = node.weights[i]
    if i_input < 0:
    	inputs.append(args[-i_input - 1] * w)
    else:
        # enter here
    	inputs.append(self.nodes[i_input].output * w)
```

We have here:

```
ipdb> pp self.nodes[i_input].output                  
173.72143238598218

ipdb> pp w                                               
-0.7139887072312956

pp self.nodes[i_input].output*w                     
-124.03514092763635

pp inputs                                                                                            [-124.03514092763635]
```

Next, evaluate the function. This is the outcome, the negative of `inputs[0]`:

```
ipdb> pp node.output                                     
124.03514092763635
```

Now, print the nodes again:

```
ipdb> print(self)                                                                                                                                             
[fitness: None
 nodes (10): 
   *0: (i_func: 4, i_inputs: [   -1,  None], weights: [-0.84,  None], i_output:  0, output:  173.72, active: True)
    1: (i_func: 2, i_inputs: [   -3,    -3], weights: [-0.42, -0.10], i_output:  1, output:    None, active: False)
    2: (i_func: 4, i_inputs: [   -1,  None], weights: [ 0.62,  None], i_output:  2, output:    None, active: False)
    3: (i_func: 3, i_inputs: [    0,     1], weights: [ 0.27,  0.69], i_output:  3, output:    None, active: False)
    4: (i_func: 1, i_inputs: [    0,     3], weights: [-0.72,  0.28], i_output:  4, output:    None, active: False)
    5: (i_func: 3, i_inputs: [   -3,     3], weights: [-0.95,  0.78], i_output:  5, output:    None, active: False)
   *6: (i_func: 4, i_inputs: [    0,  None], weights: [-0.71,  None], i_output:  6, output:  124.04, active: True)
    7: (i_func: 1, i_inputs: [    4,     3], weights: [ 0.67, -0.38], i_output:  7, output:    None, active: False)
    8: (i_func: 4, i_inputs: [    4,  None], weights: [-0.23,  None], i_output:  8, output:    None, active: False)
   *9: (i_func: 4, i_inputs: [    6,  None], weights: [-0.77,  None], i_output:  9, output:    None, active: True)
]

```

Now, evaluate the last active node:

```
*9: (i_func: 4, i_inputs: [    6,  None], weights: [-0.77,  None], i_output:  9, output:    None, active: True)
```

Notice that this node is connected with node `6`:

```python
i_func: 4,                 # function: neg
i_inputs: [    6,  None]   # ie [self.nodes[6].ouput,  None]
```

Thus, we have:

```python
for i in range(self.function_set[node.i_func].arity):
	i_input = node.i_inputs[i]
    w = node.weights[i]
    if i_input < 0:
    	inputs.append(args[-i_input - 1] * w)
    else:
        # enter here
    	inputs.append(self.nodes[i_input].output * w)
```

```
ipdb> pp self.nodes[i_input].output                                                                                                                           
124.03514092763635
ipdb> pp w                                                                                                                                                    
-0.7678325242079145
ipdb> pp self.nodes[i_input].output * w                                                                                                                       
-95.23821534895141
ipdb> pp inputs                                                                                                                                               
[-95.23821534895141]

```

This is the output, the negation of `inputs[0]`:

```
pp node.output                                     
95.23821534895141
```

Print all the nodes:

```
ipdb> print(self)                                                                                                                                             
[fitness: None
 nodes (10): 
   *0: (i_func: 4, i_inputs: [   -1,  None], weights: [-0.84,  None], i_output:  0, output:  173.72, active: True)
    1: (i_func: 2, i_inputs: [   -3,    -3], weights: [-0.42, -0.10], i_output:  1, output:    None, active: False)
    2: (i_func: 4, i_inputs: [   -1,  None], weights: [ 0.62,  None], i_output:  2, output:    None, active: False)
    3: (i_func: 3, i_inputs: [    0,     1], weights: [ 0.27,  0.69], i_output:  3, output:    None, active: False)
    4: (i_func: 1, i_inputs: [    0,     3], weights: [-0.72,  0.28], i_output:  4, output:    None, active: False)
    5: (i_func: 3, i_inputs: [   -3,     3], weights: [-0.95,  0.78], i_output:  5, output:    None, active: False)
   *6: (i_func: 4, i_inputs: [    0,  None], weights: [-0.71,  None], i_output:  6, output:  124.04, active: True)
    7: (i_func: 1, i_inputs: [    4,     3], weights: [ 0.67, -0.38], i_output:  7, output:    None, active: False)
    8: (i_func: 4, i_inputs: [    4,  None], weights: [-0.23,  None], i_output:  8, output:    None, active: False)
   *9: (i_func: 4, i_inputs: [    6,  None], weights: [-0.77,  None], i_output:  9, output:   95.24, active: True)
]

```

Notice that there are only three nodes evaluated.

 At the end of the function, we return the output of the three linked nodes:

```python
def eval(self, *args):
       # eval nodes
       return self.nodes[-1].output
```

-----

**Note** Here, I assume that the final node, that is `self.nodes[-1]` contains the computations of all the active nodes.

-----

Thus, the output of `eval()` in `Individual`is:

```
pp self.nodes[-1].output        
95.23821534895141
```

Then, we have the outcome of `eval()` in `AIBird`:

```
class AIBird(Bird):
   
    def eval(self, v, h, g):
        return self.brain.eval(v, h, g)
```

and finally, we use that value for this threshold in `game.py`:

```python
# game.py
def try_flap(self, bird):
    """
    Try to flap the bird if needed
    """
    
    # compute the tree inputs: v, h, g
    front_bottom_pipe = self._get_front_bottom_pipe(bird)
    h = front_bottom_pipe.rect.x - bird.rect.x
    v = front_bottom_pipe.rect.y - bird.rect.y
    g = front_bottom_pipe.gap
    
    if bird.eval(v, h, g) > 0:
        bird.flap()
```



This function is called as follows (**check this**):

```python
# game.py
run() -> _handle_events() -> try_flap()
```





Falta averiguar 

- como determina que nodes son active
- porque los primeros nodes solo usan negation
- como enlaza nodes
- entender el rango de `i_input`
- para que sirve `i_output`





**Ahora, haz una prueba a mano para saber el significado delas operaciones de los nodos**



Agregar `print_nodes()` antes de `eval()` para imprimir la lista de nodos en orden

Agregar una salida de referencia con los valores de fitness







## Save individual to file

Use `cgp.save_chrom()` to save the list of nodes of an individual:

```python
# game.py
def run(self):
    # draw()
    
    # save best individual
    t = self.current_generation
    filepath = "best_chrom_t_%d.pkl" % t
    tmp_pop = sorted(self.pop, key=lambda ind: ind.fitness)  # stable sorting
    best_chrom = tmp_pop[-1]
    
    # uncomment this line to save the best player of every generation
    #cgp.save_chrom(best_chrom, filepath)
```

Use `cgp.load_chrom(filepath)` to return an instance of `Individual`:

```python
# unfold.py
filepath = "best_chrom_t_6.pkl"
chrom = load_chrom(filepath)
```



## Unfold math expression



```python
# unfold.py

filepath = "best_chrom_t_6.pkl"
chrom = load_chrom(filepath)            # get instance of Individual
    
fs = ["+", "-", "*", "/", "-"]          # function set
leaves = {-1: "a", -2: "b", -3: "c"}    # terminal nodes

last_node = len(chrom.nodes)-1          # start from the last active node
result = unfold(last_node, chrom.nodes, fs, leaves)
```

This is the content of `chrom`:

```
[fitness: None
 nodes (10): 
    0: (i_func: 4, i_inputs: [   -2,  None], weights: [-0.26,  None], i_output:  0, output:    None, active: False)
   *1: (i_func: 4, i_inputs: [   -2,  None], weights: [-0.19,  None], i_output:  1, output:    7.31, active: True)
    2: (i_func: 0, i_inputs: [   -1,    -3], weights: [ 0.68, -0.47], i_output:  2, output:    None, active: False)
   *3: (i_func: 4, i_inputs: [   -3,  None], weights: [ 0.58,  None], i_output:  3, output:  -82.53, active: True)
   *4: (i_func: 0, i_inputs: [   -3,    -1], weights: [ 0.86, -0.63], i_output:  4, output:   20.63, active: True)
    5: (i_func: 3, i_inputs: [    1,     0], weights: [ 0.62, -0.35], i_output:  5, output:    None, active: False)
   *6: (i_func: 1, i_inputs: [    1,     3], weights: [-0.32,  0.31], i_output:  6, output:   23.41, active: True)
    7: (i_func: 1, i_inputs: [    4,     5], weights: [ 0.49, -0.72], i_output:  7, output:    None, active: False)
    8: (i_func: 0, i_inputs: [    1,     7], weights: [-0.84,  0.59], i_output:  8, output:    None, active: False)
   *9: (i_func: 1, i_inputs: [    6,     4], weights: [-0.56, -0.19], i_output:  9, output:   -9.30, active: True)
]
```

Here, `unfold()` returns a string representation of this graph. Nodes `-1`, `-2`, and `-3` are inputs (terminal nodes). In this case, this is the expression:

```
(((((- b * -0.19246) * -0.32020) - ((- c * 0.58119) * 0.31205)) * -0.56167) - (((c * 0.85968) + (a * -0.63010)) * -0.18670))
```

We can export this expression and use it in a javascript script along with `eval()`. See this example in Node.js, use `node` to start the interpreter:

```
> a = 10
10
> b = 20
20
> c = 30
30
> model = "(((((- b * -0.19246) * -0.32020) - ((- c * 0.58119) * 0.31205)) * -0.56167) - (((c * 0.85968) + (a * -0.63010)) * -0.18670))"
'(((((- b * -0.19246) * -0.32020) - ((- c * 0.58119) * 0.31205)) * -0.56167) - (((c * 0.85968) + (a * -0.63010)) * -0.18670))'
> eval(model)
1.2749971719038489
```













## TODO

- [x] Guardar la poblacion/un individuo como un dictionario
- [ ] Ignorar el campo `i_output`
- [ ] Pasar el diccionario a una expresion en cadena

```
ipdb> pp self.pop[0]                                                                                                                                          
[fitness: 383
 nodes (10): 
    0: (i_func: 4, i_inputs: [   -2,  None], weights: [-0.26,  None], i_output:  0, output:    None, active: False)
   *1: (i_func: 4, i_inputs: [   -2,  None], weights: [-0.19,  None], i_output:  1, output:    6.54, active: True)
    2: (i_func: 2, i_inputs: [   -1,    -3], weights: [ 0.68, -0.47], i_output:  2, output:    None, active: False)
   *3: (i_func: 4, i_inputs: [   -3,  None], weights: [ 0.58,  None], i_output:  3, output:  -66.84, active: True)
   *4: (i_func: 0, i_inputs: [   -3,    -1], weights: [ 0.86, -0.63], i_output:  4, output:   25.14, active: True)
    5: (i_func: 3, i_inputs: [    1,     2], weights: [ 0.07, -0.35], i_output:  5, output:    None, active: False)
   *6: (i_func: 1, i_inputs: [    1,     3], weights: [-0.32,  0.24], i_output:  6, output:   13.64, active: True)
    7: (i_func: 1, i_inputs: [    0,     5], weights: [ 0.49, -0.72], i_output:  7, output:    None, active: False)
    8: (i_func: 0, i_inputs: [    1,     7], weights: [-0.84,  0.59], i_output:  8, output:    None, active: False)
   *9: (i_func: 1, i_inputs: [    6,     4], weights: [-0.56, -0.19], i_output:  9, output:   -2.97, active: True)
]

ipdb> pp (v, h, g)               
(-12, 163, 137)
```







## Reference output

This is the output from the project on [bitbucket](https://bitbucket.org/auraham_cg/toptamaulipas19/src/default/):

```
In [1]: %run main.py                                                                                                                                          
pygame 1.9.6
Hello from the pygame community. https://www.pygame.org/contribute.html
libpng warning: iCCP: known incorrect sRGB profile
-------- Generation: 0. Max score so far: 0 -------
# active genes:  6
# active genes:  4
# active genes:  2
# active genes:  5
# active genes:  4
# active genes:  5
# active genes:  8
# active genes:  4
# active genes:  3
# active genes:  1
-------- Generation: 1. Max score so far: 202 -------
# active genes:  5
# active genes:  4
# active genes:  3
# active genes:  5
# active genes:  4
# active genes:  4
# active genes:  4
# active genes:  5
-------- Generation: 2. Max score so far: 202 -------
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  4
-------- Generation: 3. Max score so far: 202 -------
# active genes:  5
# active genes:  6
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
-------- Generation: 4. Max score so far: 386 -------
# active genes:  4
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
-------- Generation: 5. Max score so far: 907 -------
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
-------- Generation: 6. Max score so far: 3896 -------
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
-------- Generation: 7. Max score so far: 5609 -------
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5
# active genes:  5

```







## How to save and load solutions (pending)

In this section, we will add two functions, `save` and `load`, in `cgp.py` to save the progress of the population.



**Save best solution**

```python
def run(self):
        # ...
        t = self.current_generation
        self.pop = cgp.evolve(self.pop, pb, MU, LAMBDA, t)
```



```python
# @todo: add t
def evolve(pop, mut_rate, mu, lambda_, t):
   
    pop = sorted(pop, key=lambda ind: ind.fitness)  # stable sorting
    parents = pop[-mu:]
    
    # @add this
    # save best solution so far
    best_sol = pop[-1]
    save(best_sol, t)
    
    # generate lambda new children via mutation
    offspring = []
    for _ in range(lambda_):
        parent = random.choice(parents)
        offspring.append(parent.mutate(mut_rate))
    return parents + offspring
```



```python
def save(best_sol, t):

    # save pop every 50 generations
    if t % 50 != 0:
        return
    
    
	

```









IMPRIME UN CHROMOSOMA PARA VER SI PODEMOS ENTENDER LAS RELACIONES ENTRE NODO ACTUAL Y NODOS ANTERIORES COMO INPUT





```python
def eval(self, *args):
    """
    Given inputs, evaluate the output of this CGP individual.
    :return the final output value
    """
    if not self._active_determined:
        self._determine_active_nodes()
        self._active_determined = True
    
    # forward pass: evaluate
    for node in self.nodes:
        if node.active:
                
            import ipdb; ipdb.set_trace()
                
            inputs = []
            for i in range(self.function_set[node.i_func].arity):
                i_input = node.i_inputs[i]
                w = node.weights[i]
                if i_input < 0:
                    # args: (79, 258, 109)
                    
                    # si i_input es un entero negativo entonces
                    # 1. lo convierte en positivo
                    # 2. lo desplaza una unidad (para que comience en 0, supongo)
                    # 3. accede al argumento correspondiente en args
                    # 4. multiplica el argumento por el peso
                    # 5. agrega el valor resultante a la lista de inputs
                    inputs.append(args[-i_input - 1] * w)
                    
                    # es decir, si i_input es un entero negativo,
                    # entonces la entrada del node proviene de args
                    # ie de los tres parametros de entrada del grafo
                    # args = ()
                    # ie, la entrada del node no proviene de otro node
                    # sino de los parametros de entrada del grafo
                else:
                    
                    # si i_input es un entero positivo entonces
                    # 1. accede al valor de salida nodo i_input
                    # 2. multiplica el valor anterior por el peso
                    # 3. agrega el valor resultante a la lista de inputs
                    inputs.append(self.nodes[i_input].output * w)
                    
            # evalua los inputs de acuerdo al operador i_func        
            node.output = self.function_set[node.i_func](*inputs)
    return self.nodes[-1].output
```













## How to improve it?

- Incorporar cruza para combinar buenas soluciones
- Cambiar la actualizacion de los pesos de un modo menos drastico:

```python
def mutate(self, mut_rate=0.01):
       
	child = copy.deepcopy(self)
    for pos, node in enumerate(child.nodes):
            
    	# mutate the function gene
        if random.random() < mut_rate:
	        node.i_func = random.choice(range(len(self.function_set)))

        # mutate the input genes (connection genes)
        arity = self.function_set[node.i_func].arity
        for i in range(arity):
                
            if node.i_inputs[i] is None or random.random() < mut_rate:  
                node.i_inputs[i] = random.randint(max(pos - self.level_back, -self.n_inputs), 
                                                  pos - 1)
            
            if node.weights[i] is None or random.random() < mut_rate:
                node.weights[i] = random.uniform(self.weight_range[0], self.weight_range[1])
```

Aqui, los weights cambian drasticamente. Seria mejor un enfoque del tipo evolucion diferencial, en donde combinemos los weights de buenas soluciones



## Contact

Auraham Camacho `auraham.cg@gmail.com`