"""
Cartesian genetic programming
"""
import operator as op
import random
import copy
import math
import pickle
from settings import VERBOSE, N_COLS, LEVEL_BACK, SEED
from format_string import format_vector, format_value

# init PRNG
random.seed(SEED)

class Function:
    """
    A general function
    """
    def __init__(self, name, f, arity):
        self.name = name
        self.f = f
        self.arity = arity

    def __call__(self, *args, **kwargs):
        return self.f(*args, **kwargs)
        
    def __repr__(self):
        return "[%s,%d]" % (self.name, self.arity)


class Node:
    """
    A node in CGP graph
    """
    def __init__(self, max_arity, data=None):
        """
        Initialize this node randomly
        """
        
        if data is None:
            
            self.i_func = None
            self.i_inputs = [None] * max_arity
            self.weights = [None] * max_arity
            self.i_output = None
            self.output = None
            self.active = False
    
        else:
            
            self.i_func = data["i_func"]
            self.i_inputs = data["i_inputs"]
            self.weights = data["weights"]
            self.i_output = data["i_output"]
            self.output = data["output"]
            self.active = data["active"]
    
    
    
    def __repr__(self):
        
        str_i_inputs = format_vector(self.i_inputs)
        str_weights = format_vector(self.weights)
        str_i_output = "%2d" % self.i_output
        str_output = format_value(self.output)
        str_active = "%s" % self.active
        
        str_node = "(i_func: %s, i_inputs: %s, weights: %s, i_output: %s, output: %s, active: %s)" % (self.i_func, str_i_inputs, str_weights, str_i_output, str_output, str_active)
        
        return str_node

class Individual:
    """
    An individual (chromosome, genotype, etc.) in evolution
    """
    function_set = None
    weight_range = [-1, 1]
    max_arity = 3
    n_inputs = 3
    n_outputs = 1
    n_cols = N_COLS
    level_back = LEVEL_BACK

    def __init__(self, list_node_data=None, n_inputs=3):
        
        self.n_inputs = n_inputs
        
        if list_node_data is None:
            
            # create nodes randomly
            self.nodes = []
            for pos in range(self.n_cols):
                self.nodes.append(self._create_random_node(pos))
            for i in range(1, self.n_outputs + 1):
                self.nodes[-i].active = True
        
        else:
            
            # create nodes from data
            l = len(list_node_data)
            self.nodes = [None] * l
            
            for i in range(l):
                self.nodes[i] = Node(self.max_arity, list_node_data[i])
            
        self.fitness = None
        self._active_determined = False
        
        
    def __repr__(self):
        
        str_fitness = "None" if self.fitness is None else "%d" % self.fitness
        str_nodes = ""
        
        for i, node in enumerate(self.nodes):
            
            
            crown = "*" if node.active else " " 
            
            #i_node = "   %2d: %s\n" % (i, node)
            i_node = "   %s%d: %s\n" % (crown, i, node)
            str_nodes += i_node
        
        output = "[fitness: %s\n nodes (%d): \n%s]" % (str_fitness, len(self.nodes), str_nodes)
        
        return output

    def _create_random_node(self, pos):
        node = Node(self.max_arity)
        node.i_func = random.randint(0, len(self.function_set) - 1)
        for i in range(self.function_set[node.i_func].arity):
            node.i_inputs[i] = random.randint(max(pos - self.level_back, -self.n_inputs), pos - 1)
            node.weights[i] = random.uniform(self.weight_range[0], self.weight_range[1])
        node.i_output = pos

        return node

    def _determine_active_nodes(self):
        """
        Determine which nodes in the CGP graph are active
        """
        # check each node in reverse order
        n_active = 0
        for node in reversed(self.nodes):
            if node.active:
                n_active += 1
                for i in range(self.function_set[node.i_func].arity):
                    i_input = node.i_inputs[i]
                    if i_input >= 0:  # a node (not an input)
                        self.nodes[i_input].active = True
        if VERBOSE:
            print("# active genes: ", n_active)


    def eval(self, *args):
        """
        Given inputs, evaluate the output of this CGP individual.
        :return the final output value
        """
        
        #import ipdb; ipdb.set_trace()
        
        if not self._active_determined:
            self._determine_active_nodes()
            self._active_determined = True
        # forward pass: evaluate
        for node in self.nodes:
            if node.active:
                inputs = []
                for i in range(self.function_set[node.i_func].arity):
                    i_input = node.i_inputs[i]
                    w = node.weights[i]
                    if i_input < 0:
                        inputs.append(args[-i_input - 1] * w)
                    else:
                        inputs.append(self.nodes[i_input].output * w)
                node.output = self.function_set[node.i_func](*inputs)
        return self.nodes[-1].output

    def mutate(self, mut_rate=0.01):
        """
        Mutate this individual. Each gene is varied with probability *mut_rate*.
        :param mut_rate: mutation probability
        :return a child after mutation
        """
        child = copy.deepcopy(self)
        for pos, node in enumerate(child.nodes):
            # mutate the function gene
            if random.random() < mut_rate:
                node.i_func = random.choice(range(len(self.function_set)))
            # mutate the input genes (connection genes)
            arity = self.function_set[node.i_func].arity
            for i in range(arity):
                if node.i_inputs[i] is None or random.random() < mut_rate:  # if the mutated function requires more arguments, then the last ones are None 
                    node.i_inputs[i] = random.randint(max(pos - self.level_back, -self.n_inputs), pos - 1)
                if node.weights[i] is None or random.random() < mut_rate:
                    node.weights[i] = random.uniform(self.weight_range[0], self.weight_range[1])
            # initially an individual is not active except hte last output node
            node.active = False
        for i in range(1, self.n_outputs + 1):
            child.nodes[-i].active = True
        child.fitness = None
        child._active_determined = False
        return child


# function set
def protected_div(a, b):
    if abs(b) < 1e-6:
        return a
    return a / b


# init function set
fs = [
    Function("add", op.add, 2), 
    Function("sub", op.sub, 2), 
    Function("mul", op.mul, 2), 
    Function("div", protected_div, 2), 
    Function("neg", op.neg, 1),
    ]
        
Individual.function_set = fs
Individual.max_arity = max(f.arity for f in fs)


def evolve(pop, mut_rate, mu, lambda_):
    """
    Evolve the population *pop* using the mu + lambda evolutionary strategy

    :param pop: a list of individuals, whose size is mu + lambda. The first mu ones are previous parents.
    :param mut_rate: mutation rate
    :return: a new generation of individuals of the same size
    """
    pop = sorted(pop, key=lambda ind: ind.fitness)  # stable sorting
    parents = pop[-mu:]
    # generate lambda new children via mutation
    offspring = []
    for _ in range(lambda_):
        parent = random.choice(parents)
        offspring.append(parent.mutate(mut_rate))
    return parents + offspring


def create_population(n, n_inputs=3):
    """
    Create a random population composed of n individuals.
    """
    return [Individual(n_inputs=n_inputs) for _ in range(n)]


def save_chrom(chrom, filepath="output.pkl"):
    """
    Save chrom in a file
    
    Input
    chrom       Instance of Individual
    filepath    str, path of output file
    """
    
    nodes = []
    
    # get data from each node
    for node in chrom.nodes:
        
        node_data = {
            "i_func": node.i_func,
            "i_inputs": node.i_inputs,
            "weights": node.weights,
            "i_output": node.i_output,
            "output": node.output,
            "active": node.active,
        }
        
        nodes.append(node_data)

    # save to file
    output = open(filepath, "wb")
    pickle.dump(nodes, output)
    output.close()
    
    # debug
    print("saving %s" % filepath)
    print("reference")
    print(chrom)
    
    
def load_chrom(filepath):
    """
    Return an instance of Individual
    """
    
    pkl_file = open(filepath, "rb")
    list_node_data = pickle.load(pkl_file)
    pkl_file.close()
    
    # create instance of Individual
    ind = Individual(list_node_data)
    
    return ind
    
    
